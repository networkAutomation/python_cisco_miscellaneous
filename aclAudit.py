#!/usr/bin/env python

bad_words = ['matches', 'match']
with open('log.txt') as oldfile, open('log_new.txt', 'w') as newfile:
    for line in oldfile:
        if not any(bad_word in line for bad_word in bad_words):
            newfile.write(line)

result = []
with open('ie1extrtr01_new.txt') as fp:
    for line in fp:
        result.append(line.split(" ")[4]);

out = ["no " + i for i in result]

f = open('acl_config.txt', 'w')
for i in out:
    f.write(i +'\n')
